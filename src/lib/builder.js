var helper = require('./helper.js'),
    config = require('./config.js')(),
    write = helper.writeToFile,
    loadTemplate = helper.loadTemplate,
    loadFile = helper.loadFile,
    mkdir = helper.createDirectory,
    copyTemplate = helper.copyTemplate,
    gulpFile = loadTemplate('/server/gulpfile.js'),
    buildTask = loadTemplate('/server/gulp/tasks/build.js'),
    buildtestTask = loadTemplate('/server/gulp/tasks/buildtest.js'),
    coffeescriptTask = loadFile('/server/gulp/tasks/coffeescript.js'),
    typescriptTask = loadFile('/server/gulp/tasks/typescript.js'),
    es6Task = loadFile('/server/gulp/tasks/es6.js'),
    fontsTask = loadFile('/server/gulp/tasks/fonts.js'),
    imagesTask = loadFile('/server/gulp/tasks/images.js'),
    packagesTask = loadFile('/server/gulp/tasks/packages.js'),
    scriptsTask = loadTemplate('/server/gulp/tasks/scripts.js'),
    startTask = loadFile('/server/gulp/tasks/start.js'),
    stylesTask = loadTemplate('/server/gulp/tasks/styles.js'),
    watchTask = loadTemplate('/server/gulp/tasks/watch.js'),
    configGulp = loadTemplate('/server/gulp/config.js'),
    tsd = loadFile('/server/tsd.json'),
	gulpTsd = loadFile('/server/gulp_tsd.json'),
    builder = {},
    sass = loadFile('/css/main.scss'),
    css = loadFile('/css/main.css'),
    tsClient = loadFile('/js/app.ts'),
    jsClient = loadFile('/js/app.js'),
    coffeeClient = loadFile('/js/app.coffee'),
    index = loadFile('js/server/routes/index.js'),
    app = loadFile('/js/server/app.js'),
    bowerConfig = loadFile('/server/bower.json'),
    bowerrc = loadFile('/server/bowerrc'),
    gitIgnore = loadFile('/server/gitignore'),
    readme = loadFile('/server/README.md');

builder.packages = function packagesBuild() {
    var styleEngine = config.styleEngine,
        jsClientEngine = config.jsClientEngine,
        viewEngine = config.viewEngine;

    var pkg = {
        name: config.app_name,
        version: '1.0.0',
        private: true,
        scripts: {
            "preinstall": "npm i -g gulp && npm i -g bower",
            "startdev": "gulp start",
            "start": "node app.js",
            "postinstall": "bower install --config.interactive=false && gulp build",
            "test": "gulp buildtest && karma start karma.config.js"
        },
        dependencies: {
            'express': '^4.9.0',
            'body-parser': '^1.8.1',
            'cookie-parser': '^1.3.3',
            'morgan': '^1.3.0',
            'serve-favicon': '^2.1.3',
            'debug': '^2.0.0',
            "gulp": "^3.8.8",
            "gulp-autoprefixer": "^1.0.1",
            "gulp-concat": "^2.4.1",
            "gulp-debug": "^1.0.1",
            "gulp-filter": "^1.0.2",
            "gulp-if": "^1.2.5",
            "gulp-install": "^0.2.0",
            "gulp-minify-css": "^0.3.11",
            "gulp-order": "^1.1.1",
            "gulp-rename": "^1.2.0",
            "gulp-sass": "^0.7.3",
            "gulp-uglify": "^1.0.1",
            "opener": "^1.4.0",
            "main-bower-files": "^2.0.0",
            "merge-stream": "^0.1.5",
            "bower": "1.3.x",
            "require-dir": "^0.1.0"
        }
    };

    switch (viewEngine) {
        case 'jade':
            pkg.dependencies['jade'] = '^1.6.0';
            break;
    }

    if (jsClientEngine == 'ts') {
        pkg.dependencies["gulp-typescript"] = "^2.0.0";
        pkg.dependencies["gulp-tsd"] = "^0.0.3";
    }

    if (jsClientEngine == 'coffee' || jsClientEngine == 'es6') {
        pkg.dependencies["gulp-browserify"] = "^0.5.0";
    }

    if (jsClientEngine == 'coffee') {
        pkg.dependencies['coffeeify'] = '^1.0.0';
    }

    if (jsClientEngine == 'es6') {
        pkg.dependencies['gulp-6to5'] = '^1.0.0';
    }

    return pkg;
};

builder.gulp = function gulpBuild(path) {
    var styleEngine = config.styleEngine,
        jsClientEngine = config.jsClientEngine,
        viewEngine = config.viewEngine;

    if (jsClientEngine == 'ts') {
        write(path + '/tsd.json', tsd);
        write(path + '/gulp_tsd.json', gulpTsd);
        write(path + '/gulp/tasks/typescript.js', typescriptTask)
    }

    if (jsClientEngine == 'coffee') {
        write(path + '/gulp/tasks/coffeescript.js', coffeescriptTask);
    }

    if (jsClientEngine == 'es6') {
        write(path + '/gulp/tasks/es6.js', es6Task)
    }

    write(path + '/gulpfile.js', gulpFile({
        appName: config.app_name
    }));
    write(path + '/gulp/tasks/build.js', buildTask({
        jsEngine: jsClientEngine
    }));
    write(path + '/gulp/tasks/buildtest.js', buildtestTask({
        jsEngine: jsClientEngine
    }));
    write(path + '/gulp/tasks/fonts.js', fontsTask);
    write(path + '/gulp/tasks/images.js', imagesTask);
    write(path + '/gulp/tasks/packages.js', packagesTask);
    write(path + '/gulp/tasks/scripts.js', scriptsTask({
        jsEngine: jsClientEngine
    }));
    write(path + '/gulp/tasks/start.js', startTask);
    write(path + '/gulp/tasks/styles.js', stylesTask({
        cssEngine: styleEngine
    }));
    write(path + '/gulp/tasks/watch.js', watchTask({
        cssEngine: styleEngine,
        jsEngine: jsClientEngine
    }));
    write(path + '/gulp/config.js', configGulp({
        cssEngine: styleEngine,
        jsEngine: jsClientEngine
    }));
};

builder.buildAt = function createApplicationAt(path) { 
	var styleEngine = config.styleEngine,
        jsClientEngine = config.jsClientEngine,
        viewEngine = config.viewEngine;
	mkdir(path,function() {
		mkdir(path + '/assets');
		switch(styleEngine) {
			case 'sass':
				mkdir(path + '/assets/scss');
				write(path + '/assets/scss/main.scss', sass);
				break;
			default:
				mkdir(path + '/assets/css');
				write(path + '/assets/css/main.css', css);
				break;
		}
		switch(jsClientEngine) {
			case 'ts':
				mkdir(path + '/assets/ts');
				write(path + '/assets/ts/app.ts', tsClient);
				break;
			case 'coffee':
				mkdir(path + '/assets/coffee');
				write(path + '/assets/coffee/app.coffee');
			default:
				mkdir(path + '/assets/js');
				write(path + '/assets/js/app.js', jsClient);
				break;
		}

		mkdir(path + '/routes', function(){
			write(path + '/routes/index.js', index);
		});

		mkdir(path + '/views', function(){
			switch(viewEngine) {
				case 'jade':
				default:
					copyTemplate('jade/index.jade', path + '/views/index.jade');
					copyTemplate('jade/layout.jade', path + '/views/layout.jade');
					copyTemplate('jade/error.jade', path + '/views/error.jade');
					break;
			}
		});
		mkdir(path + '/views/templates');

		app = app.replace('{viewEngine}', viewEngine);
		app = app.replace('{appName}', config.app_name);

		mkdir(path + '/gulp', function(){
			mkdir(path + '/gulp/tasks', function(){
				builder.gulp(path);
			});
		});

		write(path + '/package.json', JSON.stringify(builder.packages(), null, 2));
		write(path + '/app.js', app);
		write(path + '/.bowerrc', bowerrc);
		write(path + '/bower.json', bowerConfig.replace('{appName}', config.app_name));
		write(path + '/.gitignore', gitIgnore);
		write(path + '/README.md', readme.replace('{appName}', config.app_name));
	});
};


module.exports = builder;
