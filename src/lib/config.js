var config = {},
	path = require('path');

module.exports = function(program) {
    if (!program) {
        return config;
    }
    config.styleEngine = program.css || 'css',
	config.jsClientEngine = program.jsClient || 'js',
	config.viewEngine = program.view || 'jade';
	config.destination_path = program.args.shift() || '.';
	config.appname = path.basename(path.resolve(config.destination_path));
	return config;
};
