var fs = require('fs'),
	mkdirp = require('mkdirp'),
	handlebars = require('handlebars'),
	path = require('path');

handlebars.registerHelper('if_eq', function(a, b, opts) {
	if(a == b) // Or === depending on your needs
		return opts.fn(this);
	else
		return opts.inverse(this);
});

helper = {
	loadFile: function load_file(name) {
		return fs.readFileSync(path.join(__dirname, '..', 'templates', name), 'utf-8');
	},
	loadTemplate: function load_template(name) {
		return handlebars.compile(fs.readFileSync(path.join(__dirname, '..', 'templates', name), 'utf-8'));
	},
	emptyDirectory: function emptyDirectory(path, fn) {
		fs.readdir(path, function(err, files){
			if (err && 'ENOENT' != err.code) throw err;
			fn(!files || !files.length);
		});
	},
	createDirectory: function mkdir(path, fn) {
		mkdirp(path, 0755, function(err){
			if (err) throw err;
			console.log('   \033[36mcreate\033[0m : ' + path);
			fn && fn();
		});
	},
	writeToFile: function write(path, str, mode) {
		fs.writeFile(path, str, { mode: mode || 0666 });
		console.log('   \x1b[36mcreate\x1b[0m : ' + path);
	},
	abort: function abort(str) {
		console.error(str);
		process.exit(1);
	},
	copyTemplate: function copy_template(from, to) {
		from = path.join(__dirname, '..', 'templates', from);
		helper.writeToFile(to, fs.readFileSync(from, 'utf-8'));
	}
};

module.exports = helper;