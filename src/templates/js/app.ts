/// <reference path="typings/tsd.d.ts" />
module App {
    export class Main {
        public static init() {
            angular.module('app-main', [])

            angular.bootstrap(document, ['app-main']);
        }
    }
}

$(function() {
	App.Main.init();
});
