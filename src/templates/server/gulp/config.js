module.exports = {
  js: {
		vendorConcatTo: 'vendor.js',
		src: ['assets/js/**/*js', 'assets/js/*js'],
		dest: 'public/js',{{#if_eq jsEngine 'es6'}}
		concatTo: 'app.js',{{/if_eq}}
		customPaths: []
	},
	{{#if_eq jsEngine 'ts'}}
	ts: {
		src: ['assets/ts/*ts', 'assets/ts/**/*ts'],
		defDest: 'public/definitions',
		dest: 'public/js',
		concatTo: 'app.js'
	},
	{{/if_eq}}
	{{#if_eq jsEngine 'coffee'}}
	coffee: {
		src: ['assets/coffee/*coffee', 'assets/coffee/**/*coffee'],
		defDest: 'public/definitions',
		dest: 'public/js',
		concatTo: 'app.js'
	},
	{{/if_eq}}
	css: {
		vendorConcatTo: 'vendor.css',
		concatTo: 'app.css',
		src: 'assets/css/*css',
		dest: 'public/css'
	},
	{{#if_eq cssEngine 'sass'}}
	scss: {
		src: 'assets/scss/*scss'
	},
	{{/if_eq}}
	fonts: {
		src: ['assets/vendor/bootstrap/dist/fonts/*',
			'assets/vendor/font-awesome/fonts/*',
			'assets/fonts/*'
		],
		dest: 'public/fonts'
	},
	images: {
		src: "assets/images/*",
		dest: "public/images"
	},
	tests: {
		mainDest: 'tests/main',
		vendorDest: 'tests/vendor',
		dest: "tests"
	}
};