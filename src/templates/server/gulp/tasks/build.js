var gulp = require('gulp');

gulp.task('build', ['scripts', {{#if_eq jsEngine 'ts'}}'typescript',{{/if_eq}}{{#if_eq jsEngine 'coffee'}}'coffeescript',{{/if_eq}}{{#if_eq jsEngine 'es6'}}'es6',{{/if_eq}} 'styles', 'fonts', 'images'], function() {});