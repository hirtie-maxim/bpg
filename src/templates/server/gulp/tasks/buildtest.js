var gulp = require('gulp'),
	paths = require('../config.js')
	gulpFilter = require('gulp-filter'),
	mainBower = require('main-bower-files'),
	merge = require('merge-stream'),
	concat = require('gulp-concat'),{{#if_eq jsEngine 'ts'}}
  ts = require('gulp-typescript'),
  tsd = require('gulp-tsd'),{{/if_eq}}


gulp.task('buildtest', function() {
	var mocks = gulp.src(mainBower())
		.pipe(gulpFilter(['**/angular-mocks.js']))
		.pipe(gulp.dest(paths.tests.dest));
	var bowerFiles = gulp.src(mainBower())
		.pipe(gulpFilter(['**/*.js', '!**/*.min.js', '!**/angular-mocks.js']))
		.pipe(concat(paths.js.vendorConcatTo))
		.pipe(gulp.dest(paths.tests.vendorDest));
	var tsResult = gulp.src(paths.ts.src)
		.pipe(ts({
			declarationFiles: true,
			noLib: false,
			target: 'ES5',
			module: 'commonjs'
		}));
	var tsFiles = tsResult.js
		.pipe(concat(paths.ts.concatTo))
		.pipe(gulp.dest(paths.tests.mainDest));
	var customFiles = gulp.src(paths.js.customPaths)
		.pipe(gulp.dest(paths.tests.mainDest));
	return merge(bowerFiles, customFiles, mocks, tsFiles);
});