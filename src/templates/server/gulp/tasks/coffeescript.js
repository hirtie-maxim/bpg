var gulp = require('gulp'),
    paths = require('../config.js'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    concat = require('gulp-concat'),
    browserify = require('gulp-browserify'),
    coffee = require('gulp-coffee');

gulp.task('coffeescript', function(){
  return gulp.src(paths.coffee.src, {read: false})
    .pipe(browserify({
      transform: ['coffeeify'],
      extensions: ['.coffee']
    }))
    .pipe(concat(paths.coffee.concatTo))
    .pipe(gulpif(isProduction, uglify({
      mangle: false
    })))
    .pipe(gulp.dest(paths.coffee.dest));
});