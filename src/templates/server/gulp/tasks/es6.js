var gulp = require('gulp'),
    paths = require('../config.js'),
    concat = require('gulp-concat'),
    to5 = require('gulp-6to5');

gulp.task('es6', function() {
  return gulp.src('assets/js/app.js')
    .pipe(to5())
    .pipe(concat(paths.js.concatTo))
    .pipe(gulp.dest(paths.js.dest));
});