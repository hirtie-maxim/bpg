var gulp = require('gulp'),
		paths = require('../config.js');

gulp.task('fonts', function() {
	return gulp.src(paths.fonts.src)
		.pipe(gulp.dest(paths.fonts.dest));
});