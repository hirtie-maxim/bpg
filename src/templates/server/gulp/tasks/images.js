var gulp = require('gulp'),
		paths = require('../config.js');

gulp.task('images', function() {
	return gulp.src(paths.images.src)
		.pipe(gulp.dest(paths.images.dest));
});