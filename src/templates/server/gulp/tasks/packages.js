var gulp = require('gulp'),
	install = require("gulp-install");

gulp.task('packages', function() {
	return gulp.src(['bower.json'])
		.pipe(install());
});