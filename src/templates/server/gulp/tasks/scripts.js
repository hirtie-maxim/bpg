var gulp = require('gulp'),
    paths = require('../config.js'),
    mainBower = require('main-bower-files'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    concat = require('gulp-concat'),
    merge = require('merge-stream'),
    gulpFilter = require('gulp-filter'),
    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
    isProduction = env === "production";

gulp.task('scripts', function() {
  var bowerFiles = gulp.src(mainBower())
    .pipe(gulpFilter(['**/*.js', '!**/*.min.js', '!**/angular-mocks.js']))
    .pipe(gulpif(isProduction, uglify({
      mangle: false
    })))
    .pipe(concat(paths.js.vendorConcatTo))
    .pipe(gulp.dest(paths.js.dest));
  var customFiles = {{#if_eq jsEngine 'es6'}}gulp.src(paths.js.customPaths){{/if_eq}}{{#if_eq jsEngine 'js'}}merge(gulp.src(paths.js.src), gulp.src(paths.js.customPaths)){{/if_eq}}
    .pipe(gulpif(isProduction, uglify({
      mangle: false
    })))
    .pipe(gulp.dest(paths.js.dest));
  return merge(bowerFiles, customFiles);
});