var gulp = require('gulp'),
    debug = require('gulp-debug');

gulp.task('start', ['packages', 'build', 'watch'], function() {
  var debug = require('debug')('testing'),
    app = require('../../app'),
    opener = require('opener');

  app.set('port', process.env.PORT || 3000);

  var server = app.listen(app.get('port'), function() {
    var address = 'http://localhost:' + app.get('port');
    opener(address);
    debug('Express server listening on port ' + app.get('port'));
  });
});