var gulp = require('gulp'),
	paths = require('../config.js'),
	mainBower = require('main-bower-files'),
	gulpFilter = require('gulp-filter'),
	concat = require('gulp-concat'),
	merge = require('merge-stream'),
	autoprefixer = require('gulp-autoprefixer'),
	gulpif = require('gulp-if'),{{#if_eq cssEngine 'sass'}}
	sass = require('gulp-sass'),{{/if_eq}}
	cssMinify = require('gulp-minify-css'),
	env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
	isProduction = env === "production";

gulp.task('styles', function() {
	var bowerFiles = gulp.src(mainBower())
		.pipe(gulpFilter(['**/*.css', '!**/ng-table.css']))
		.pipe(concat(paths.css.vendorConcatTo))
		.pipe(gulp.dest(paths.css.dest));
	var assetsCssFiles = gulp.src(paths.css.src);
	{{#if_eq cssEngine 'sass'}}var assetsScssFiles = gulp.src(paths.scss.src).pipe(sass());{{/if_eq}}
	var assetsFiles = merge(assetsCssFiles{{#if_eq cssEngine 'sass'}}, assetsScssFiles{{/if_eq}})
		.pipe(concat(paths.css.concatTo))
		.pipe(autoprefixer());
	return merge(assetsFiles, bowerFiles)
		.pipe(gulpif(isProduction, cssMinify({keepBreaks:true})))
		.pipe(gulp.dest(paths.css.dest));
});