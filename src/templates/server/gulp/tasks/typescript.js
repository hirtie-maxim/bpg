var gulp = require('gulp'),
    paths = require('../config.js'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    concat = require('gulp-concat'),
    gulpFilter = require('gulp-filter'),
    ts = require('gulp-typescript'),
    tsd = require('gulp-tsd');

gulp.task('typescript', ['tsd'], function(){
  var tsResult = gulp.src(paths.ts.src)
    .pipe(ts({
      declarationFiles: true,
      noLib: false,
      target: 'ES5',
      module: 'commonjs'
    }));
  tsResult.dts.pipe(gulp.dest(paths.ts.defDest));
  return tsResult.js
    .pipe(concat(paths.ts.concatTo))
    .pipe(gulpif(isProduction, uglify({
      mangle: false
    })))
    .pipe(gulp.dest(paths.ts.dest));
});

gulp.task('tsd', function(){
  return gulp.src('./gulp_tsd.json').pipe(tsd());
});