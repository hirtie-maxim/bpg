var gulp = require('gulp');

gulp.task('watch', function() {
	gulp.watch([{{#if_eq cssEngine 'sass'}}'assets/scss/*scss',{{/if_eq}} 'assets/css/*css'], ['styles']);
	gulp.watch(['assets/js/*js', 'assets/js/**/*js'], ['scripts'{{#if_eq jsEngine 'es6'}}, 'es6'{{/if_eq}}]);{{#if_eq jsEngine 'ts'}}
	gulp.watch(['assets/ts/*ts', 'assets/ts/**/*ts'], ['typescript']);{{/if_eq}}{{#if_eq jsEngine 'coffee'}}
	gulp.watch(['assets/coffee/*coffee', 'assets/coffee/**/*coffee'], ['coffeescript']);{{/if_eq}}
	gulp.watch(['assets/fonts/*'], ['fonts']);
	gulp.watch(['bower.json'], ['build']);
});
