var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
		isProduction = env === "production";

console.log(env);

var requireDir = require('require-dir');
// Require all tasks in gulp/tasks, including subfolders
requireDir('./gulp/tasks', { recurse: true });